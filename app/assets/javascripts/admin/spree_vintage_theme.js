//= require admin/spree_core

$(document).ready(function(){
  $("#link_new_image").click(function(){
    $("#dlg_new_image").dialog("open");
  });

  $("#dlg_new_image").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
    }
  });

  $("#dlg_edit_theme_color").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
    }
  });
});
