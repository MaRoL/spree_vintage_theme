//= require store/spree_core
//= require jquery-ui

$(document).ready(function(){
  $('#slider').nivoSlider({effect: 'fade', pauseOnHover: false, randomStart: true, controlNav: false, galleries: true, animSpeed: 1000 });

  // NOTE for _header.html.erb
  $('#more_menu').click(function(){
    $("#dlg_more_menu").dialog("open");
  });

  $("#dlg_more_menu").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
    }
  });
});
