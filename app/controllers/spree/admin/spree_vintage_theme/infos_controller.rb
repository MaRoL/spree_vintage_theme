class Spree::Admin::SpreeVintageTheme::InfosController < Spree::Admin::BaseController
  def index
    @infos = Spree::SpreeVintageTheme::Info.all
  end

  def edit
    @info = Spree::SpreeVintageTheme::Info.find(params[:id])
  end

  def update
    @info = Spree::SpreeVintageTheme::Info.find(params[:id])
    if @info.update_attributes(params[:spree_vintage_theme_info])
      redirect_to admin_spree_vintage_theme_infos_path, :notice => "#{t("fat_footer.#{@info.title}")} #{t(:updated)}"
    else
      render action: "edit", :error => @info.errors
    end
  end
end
