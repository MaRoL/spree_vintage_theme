module Spree
  module Admin
    class SpreeVintageTheme::ThemeColorsController < Spree::Admin::BaseController
      include Spree::Admin::SpreeVintageTheme::ThemeColorsHelper

      def update_theme
        @old_theme = Spree::SpreeVintageTheme::ThemeColor.where(:used => "true").first
        @new_theme = Spree::SpreeVintageTheme::ThemeColor.find(params[:theme_color])
        @new_theme.used = "true"

        unless @old_theme.nil?
          @old_theme.used = "false"
          if @old_theme.save && @new_theme.save
            change_color(@old_theme, @new_theme)
            redirect_to spree.admin_spree_vintage_theme_theme_images_path, :notice => (I18n.t "messages.succeed")
          else
            redirect_to spree.admin_spree_vintage_theme_theme_images_path, :notice => (I18n.t "messages.failed")
          end
        else
          if @new_theme.save
            change_color(@old_theme, @new_theme)
            redirect_to spree.admin_spree_vintage_theme_theme_images_path, :notice => (I18n.t "messages.succeed")
          else
            redirect_to spree.admin_spree_vintage_theme_theme_images_path, :notice => (I18n.t "messages.failed")
          end
        end
      end
    end
  end
end
