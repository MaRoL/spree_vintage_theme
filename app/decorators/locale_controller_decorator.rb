module Spree
  LocaleController.class_eval do
    def change_locale
      @notice = ""
      @error = ""
      if params[:locale] && I18n.available_locales.include?(params[:locale].to_sym)
        session[:locale] = I18n.locale = params[:locale].to_sym
        @notice = t(:locale_changed)
      else
        @error = t(:locale_not_changed)
      end
      respond_to do |format|
        format.json { render :json => { :notice => @notice, :error => @error } }
      end
    end
  end
end
