module Spree
  TaxonsController.class_eval do
    before_filter :show_from_detail, :only => :show

    def show_from_detail
      if Product.find_by_permalink(params[:id]).nil?
        params[:id] = params[:id]
      else
        params[:id] = Product.find_by_permalink(params[:id]).taxons.first.permalink
      end
    end
  end
end
