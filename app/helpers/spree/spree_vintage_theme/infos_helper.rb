module Spree::SpreeVintageTheme::InfosHelper
  def load_new_variable
    @contact_us = Spree::SpreeVintageTheme::Info.find_by_title("contact_us")
    @how_to_shop = Spree::SpreeVintageTheme::Info.find_by_title("how_to_shop")
    @confirm_transfer = Spree::SpreeVintageTheme::Info.find_by_title("confirm_transfer")
  end

  def check_locales(locale)
    find = false
    I18n.available_locales.each do |l|
      if locale.to_s == l.to_s
        find = true
        break
      end
    end
    return find
  end
end
