class Spree::SpreeVintageTheme::ThemeColor < ActiveRecord::Base
  self.table_name = 'spree_spree_vintage_theme_theme_colors'
  attr_accessible :color, :filename, :used
end
