class Spree::SpreeVintageTheme::ThemeImage < ActiveRecord::Base
  self.table_name = 'spree_spree_vintage_theme_theme_images'
  attr_accessible :description, :enabled, :image, :url_destination

  has_attached_file :image, :styles => { :small => "200x200>", :thumb => "100x100>", :banner => "1014X500>" }
  # validate :file_dimensions, :unless => "errors.any?", :on => :create

  def file_dimensions
    dimensions = Paperclip::Geometry.from_file(image.queued_for_write[:original].path)
    # width  : 1024px
    # height : 510px
    if dimensions.width < 974 || dimensions.width > 1700
      errors.add(:file, (I18n.t "messages.theme_images.range_width", :range_1 => "974", :range_2 => "1700"))
    end

    if dimensions.height < 460 || dimensions.height > 1100
      errors.add(:file, (I18n.t "messages.theme_images.range_height", :range_1 => "460", :range_2 => "510"))
    end
  end
end
