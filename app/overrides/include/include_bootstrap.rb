Deface::Override.new(:virtual_path  => "spree/layouts/spree_application",
                     :insert_bottom => "[data-hook='inside_head']",
                     :name          => "bootstrap_include",
                     :text          => "<br/><%= stylesheet_link_tag 'bootstrap/css/bootstrap.css' %>
                                        <br/><%= javascript_include_tag 'bootstrap/js/bootstrap.js' %>")
