Deface::Override.new(:virtual_path     => "spree/orders/edit",
                     :replace_contents => "p#clear_cart_link",
                     :name             => "remove_or",
                     :partial          => "spree/orders/link_continue_and_empty")

Deface::Override.new(:virtual_path  => "spree/orders/edit",
                     :insert_bottom => "div#empty-cart",
                     :name          => "insert_new_class_and_id",
                     :partial       => "spree/orders/insert_class_and_id_cart")
