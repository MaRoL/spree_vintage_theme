Deface::Override.new(:virtual_path  => "spree/products/index",
                     :insert_bottom => "[data-hook='homepage_sidebar_navigation']",
                     :name          => "css_sidebar",
                     :partial       => "spree/products/fix_css_sidebar")

Deface::Override.new(:virtual_path => "spree/shared/_products",
                     :insert_top   => "[data-hook='products_list_item']",
                     :name         => "promotion_ribbon",
                     :partial      => "spree/products/promotion_ribbon")
