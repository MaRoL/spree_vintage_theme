Deface::Override.new(:virtual_path  => "spree/products/show",
                     :insert_before => "[data-hook='product_show']",
                     :name          => "insert_new_productcaption",
                     :partial       => "spree/products/new_productcaption")

Deface::Override.new(:virtual_path => "spree/products/show",
                     :insert_after => "[data-hook='product_show']",
                     :name         => "hide_product_caption",
                     :partial      => "spree/products/hide_product_caption")

Deface::Override.new(:virtual_path => "spree/products/_cart_form",
                     :remove       => "div#product-price",
                     :name         => "delete_old_product_price")

Deface::Override.new(:virtual_path => "spree/products/show",
                     :insert_top   => "div#main-image",
                     :name         => "promotion_ribbon_show",
                     :partial      => "spree/products/promotion_ribbon_show")

Deface::Override.new(:virtual_path => "spree/products/_promotions",
                     :remove       => "div#promotions",
                     :name         => "remove_promotion")
