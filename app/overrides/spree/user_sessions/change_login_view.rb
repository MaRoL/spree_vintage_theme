Deface::Override.new(:virtual_path => "spree/shared/_login",
                     :insert_after => "div#password-credentials",
                     :name         => "insert_forgot_password",
                     :partial      => "spree/shared/insert_forgot_password")
