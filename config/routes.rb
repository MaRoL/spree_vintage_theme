Spree::Core::Engine.routes.draw do
  namespace :spree do
    namespace :spree_vintage_theme do
      resources :theme_colors
      resources :theme_images
    end
  end

  scope :module => 'spree_vintage_theme' do
    resources :infos
    match "order_information" => "infos#order_information", :via => :get, :as => "order_information"
    match "search_order" => "infos#search_order", :via => :post, :as => "search_order"
  end

  namespace :admin do
    namespace :spree_vintage_theme do
      resources :theme_images
      resources :theme_colors
      resources :infos
    end
  end

  namespace :admin, :scope => "spree_vintage_theme/theme_colors" do
    match "edit_theme" => "spree_vintage_theme/theme_colors#edit_theme", :via => :post, :as => "edit_theme"
    match "update_theme" => "spree_vintage_theme/theme_colors#update_theme", :via => :post, :as => "update_theme"
  end

  namespace :admin, :scope => "spree_vintage_theme/theme_images" do
    match "enabled_selected" => "spree_vintage_theme/theme_images#enabled_selected", :via => :post, :as => "enabled_selected"
    match "redirect_enabled_selected" => "spree_vintage_theme/theme_images#redirect_enabled_selected", :via => :get, :as => "redirect_enabled"
  end

  get "/best_seller/" => "products#best_product"
  get "/new/" => "products#newest_product"
  get "/promo/" => "products#promotion_product"

  match "locale/change" => "locale#change_locale", :as => "change_locale"
end
