class CreateSpreeSpreeVintageThemeThemeImages < ActiveRecord::Migration
  def change
    create_table :spree_spree_vintage_theme_theme_images do |t|
      t.has_attached_file :image
      t.string :enabled, :default => "false"
      t.string :url_destination, :default => "#"
      t.string :description

      t.timestamps
    end
  end
end
