if Spree::SpreeVintageTheme::Info.count == 0
  if defined? Spree::GlabsPaymentMethod
    info = Spree::SpreeVintageTheme::Info.new
    info.title = "confirm_transfer"
    info.save
  end

  info = Spree::SpreeVintageTheme::Info.new
  info.title = "contact_us"
  info.save

  info = Spree::SpreeVintageTheme::Info.new
  info.title = "how_to_shop"
  info.save
end

if Spree::SpreeVintageTheme::ThemeColor.count == 0
  theme_color = Spree::SpreeVintageTheme::ThemeColor.new
  theme_color.color = "default(brown)"
  theme_color.filename = "default"
  theme_color.used = "true"
  theme_color.save

  theme_color = Spree::SpreeVintageTheme::ThemeColor.new
  theme_color.color = "blue"
  theme_color.filename = "blue"
  theme_color.save

  theme_color = Spree::SpreeVintageTheme::ThemeColor.new
  theme_color.color = "bubblegum"
  theme_color.filename = "bubblegum"
  theme_color.save

  theme_color = Spree::SpreeVintageTheme::ThemeColor.new
  theme_color.color = "orange_gee"
  theme_color.filename = "orange_gee"
  theme_color.save

  theme_color = Spree::SpreeVintageTheme::ThemeColor.new
  theme_color.color = "botany_green"
  theme_color.filename = "botany_green"
  theme_color.save
end

if Spree::SpreeVintageTheme::ThemeImage.count == 0
  @no_image = File.open("public/spree_vintage_theme/no_image.jpg")
  theme_image = Spree::SpreeVintageTheme::ThemeImage.create(:image => @no_image)
  theme_image.enabled = "true"
  theme_image.save
end
