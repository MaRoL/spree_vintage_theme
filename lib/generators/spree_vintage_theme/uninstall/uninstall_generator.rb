module SpreeVintageTheme
  module Generators
    class UninstallGenerator < Rails::Generators::Base
      source_root File.expand_path('../../../../../../db/migrate/', __FILE__)

      def delete_require_css
        gsub_file("./app/assets/stylesheets/store/all.css", /\*= require store\/spree_vintage_theme(\/.*)*/, "")
        @file = File.readlines('./app/assets/stylesheets/store/all.css')
        @file.each_with_index do |f, index|
          @file.delete_at(index) if (f =~ /^\s$/) && index > 4
        end
        File.open('./app/assets/stylesheets/store/all.css', 'w+') { |f| f.write @file.join() }
      end

      def delete_require_js
        gsub_file("./app/assets/javascripts/store/all.js", /\/\/= require store\/spree_vintage_theme(\/.*)*/, "")
        @file = File.readlines('./app/assets/javascripts/store/all.js')
        @file.each_with_index do |f, index|
          @file.delete_at(index) if (f =~ /^\s$/) && index > 10
        end
        File.open('./app/assets/javascripts/store/all.js', 'w+') { |f| f.write @file.join() }

        gsub_file("./app/assets/javascripts/admin/all.js", /\/\/= require admin\/spree_vintage_theme(\/.*)*/, "")
        @file = File.readlines('./app/assets/javascripts/admin/all.js')
        @file.each_with_index do |f, index|
          @file.delete_at(index) if (f =~ /^\s$/) && index > 10
        end
        File.open('./app/assets/javascripts/admin/all.js', 'w+') { |f| f.write @file.join() }
      end

      def remove_locale
        remove_dir "./config/locales/spree_vintage_theme/"
      end

      def drop_table
        say_status :drop, "tables"

        if ActiveRecord::Base.connection.table_exists? 'spree_spree_vintage_theme_theme_images'
          ActiveRecord::Base.connection.drop_table :spree_spree_vintage_theme_theme_images
        end
        if ActiveRecord::Base.connection.table_exists? 'spree_spree_vintage_theme_theme_colors'
          ActiveRecord::Base.connection.drop_table("spree_spree_vintage_theme_theme_colors")
        end
        if ActiveRecord::Base.connection.table_exists? 'spree_spree_vintage_theme_infos'
          ActiveRecord::Base.connection.drop_table("spree_spree_vintage_theme_infos")
        end

        @find_files = Dir.glob('./db/migrate/*_spree_vintage_theme_*')
        for file in @find_files
          File.delete(file)
        end
        puts "Remove spree_vintage_theme from Gemfile."
      end
    end
  end
end
